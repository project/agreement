<?php

namespace Drupal\agreement\Entity;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Agreement entity delete form.
 */
class AgreementDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to permanently delete this agreement? All agreement records will be removed.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.agreement.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $agreement_name = $this->entity->label();
      $this->entity->delete();
      $this->messenger()->addStatus($this->t('Successfully deleted agreement, %label', [
        '%label' => $agreement_name,
      ]));
      $form_state->setRedirect('entity.agreement.collection');
    }
    catch (EntityStorageException $e) {
      $this->messenger()->addError($this->t('An error occurred deleting the agreement.'));
    }
  }

}
