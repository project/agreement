<?php

namespace Drupal\agreement\EventSubscriber;

use Drupal\agreement\AgreementHandlerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Checks if the current user is required to accept an agreement.
 */
class AgreementSubscriber implements EventSubscriberInterface {

  use RedirectDestinationTrait;

  /**
   * Agreement handler.
   *
   * @var \Drupal\agreement\AgreementHandlerInterface
   */
  protected $handler;

  /**
   * Current path getter because paths > routes for users.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $pathStack;

  /**
   * Session manager.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * Current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Initialize method.
   *
   * @param \Drupal\agreement\AgreementHandlerInterface $agreementHandler
   *   The agreement.handler.
   * @param \Drupal\Core\Path\CurrentPathStack $pathStack
   *   The current.path.
   * @param \Drupal\Core\Session\SessionManagerInterface $sessionManager
   *   The session_manager service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current_user service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $destination
   *   The redirect.destination service.
   */
  public function __construct(AgreementHandlerInterface $agreementHandler, CurrentPathStack $pathStack, SessionManagerInterface $sessionManager, AccountInterface $account, RedirectDestinationInterface $destination) {
    $this->handler = $agreementHandler;
    $this->pathStack = $pathStack;
    $this->sessionManager = $sessionManager;
    $this->account = $account;
    $this->setRedirectDestination($destination);
  }

  /**
   * Check if the user needs to accept an agreement.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent|\Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The response event.
   */
  public function checkForRedirection($event) {
    // Users with the bypass agreement permission are always excluded from any
    // agreement.
    if (!$this->account->hasPermission('bypass agreement')) {
      $path = $this->pathStack->getPath($event->getRequest());
      $info = $this->handler->getAgreementByUserAndPath($this->account, $path);
      if ($info) {
        // Save intended destination.
        if (!isset($_SESSION['agreement_destination'])) {
          // Removes the base path from the destination.
          $base_path = $event->getRequest()->getBasePath();
          $destination = $this->getRedirectDestination()->get();
          if ($base_path !== '/' && substr($destination, 0, strlen($base_path)) === $base_path) {
            $destination = substr($destination, strlen($base_path));
          }

          $_SESSION['agreement_destination'] = $destination;
        }

        // Redirect to the agreement page.
        $route_name = 'agreement.' . $info->id();
        $redirect_url = Url::fromRoute($route_name, [
          'agreement' => $info->id(),
        ], [
          'absolute' => TRUE,
          // 'query' => $this->getDestinationArray(),
        ]);
        $event->setResponse(new LocalRedirectResponse($redirect_url->toString()));
      }
    }
  }

  /**
   * Performs redirect for access denied exceptions.
   *
   * In case the user has no permission to access a page, the denied exception
   * will be thrown. Therefore the response will be set before executing of
   * the checkForRedirection function, that will lead to an infinite redirect
   * loop.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The response exception event.
   */
  public function exceptionRedirect(ExceptionEvent $event) {
    $exception = $event->getThrowable();
    if ($exception instanceof HttpExceptionInterface && $exception->getStatusCode() === 403) {
      $this->checkForRedirection($event);
    }
  }

  /**
   * Executes function to set redirect response if it is required.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The response event.
   */
  public function requestForRedirection(RequestEvent $event) {
    $this->checkForRedirection($event);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    // Dynamic page cache will redirect to a cached page at priority 27.
    $events[KernelEvents::REQUEST][] = ['requestForRedirection', 28];
    $events[KernelEvents::EXCEPTION][] = ['exceptionRedirect', 1];

    return $events;
  }

}
