<?php

namespace Drupal\agreement;

use Drupal\agreement\Entity\Agreement;

/**
 * Agreement handler interface with additional methods.
 *
 * Methods from AgreementHandlerInterface will be ported over here and that
 * interface will be deprecated once complete.
 */
interface AgreementHandlerExtendedInterface extends AgreementHandlerInterface {

  /**
   * Determines if an agreement applies to a path.
   *
   * @param \Drupal\agreement\Entity\Agreement $agreement
   *   The agreement to check.
   * @param string $path
   *   The current path.
   *
   * @return bool
   *   TRUE if the agreement applies to the given path based on its visibility
   *   settings.
   */
  public function agreementAppliesToPath(Agreement $agreement, string $path): bool;

}
