<?php

namespace Drupal\agreement;

use Drupal\agreement\Entity\Agreement;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Agreement handler provides methods for looking up agreements.
 */
final class AgreementHandler implements AgreementHandlerExtendedInterface {

  /**
   * Prefix to use for cookie names for anonymous agreements.
   */
  const ANON_AGREEMENT_COOKIE_PREFIX = 'agreement_anon_';

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The datetime.time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal core site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Initialize method.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Site\Settings $settings
   *   The settings service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   *   The path matcher service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime.time service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module_handler service.
   */
  public function __construct(
    Connection $connection,
    Settings $settings,
    EntityTypeManagerInterface $entityTypeManager,
    PathMatcherInterface $pathMatcher,
    TimeInterface $time,
    RequestStack $requestStack,
    ModuleHandlerInterface $moduleHandler,
  ) {
    $this->connection = $connection;
    $this->settings = $settings;
    $this->entityTypeManager = $entityTypeManager;
    $this->pathMatcher = $pathMatcher;
    $this->time = $time;
    $this->requestStack = $requestStack;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function agree(Agreement $agreement, AccountInterface $account, $agreed = 1) {
    if ($this->isAnonymousAgreement($agreement, $account)) {
      return $this->agreeAnonymously($account, $agreement, $agreed);
    }
    return $this->agreeWhileLoggedIn($account, $agreement, $agreed);
  }

  /**
   * {@inheritdoc}
   */
  public function hasAgreed(Agreement $agreement, AccountInterface $account) {
    if ($this->isAnonymousAgreement($agreement, $account)) {
      return $this->hasAnonymousUserAgreed($agreement);
    }
    return $this->hasAuthenticatedUserAgreed($agreement, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function lastAgreed(Agreement $agreement, UserInterface $account) {
    $query = $this->connection->select('agreement');
    $query
      ->fields('agreement', ['agreed_date'])
      ->condition('uid', $account->id())
      ->condition('type', $agreement->id())
      ->range(0, 1);

    $agreed_date = $query->execute()->fetchField();
    return $agreed_date === FALSE || $agreed_date === NULL ? -1 : $agreed_date;
  }

  /**
   * {@inheritdoc}
   */
  public function canAgree(Agreement $agreement, AccountInterface $account) {
    return !$account->hasPermission('bypass agreement') &&
      $agreement->accountHasAgreementRole($account);
  }

  /**
   * {@inheritdoc}
   */
  public function isAnonymousAgreement(Agreement $agreement, AccountInterface $account) {
    if ($account->isAnonymous() && in_array(RoleInterface::ANONYMOUS_ID, $agreement->getSettings()['roles'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getAgreementByUserAndPath(AccountInterface $account, $path) {
    $agreement_types = $this->entityTypeManager->getStorage('agreement')->loadMultiple();
    $public_file_path = $this->settings->get('file_public_path');

    $default_exceptions = [
      '/user/password',
      '/user/register',
      '/user/reset/*',
      '/user/login',
      '/user/logout',
      '/user/logout/*',
      '/admin/config/people/agreement',
      '/admin/config/people/agreement/*',
      '/admin/config/people/agreement/manage/*',
    ];

    // Adds the public file path to the default exceptions.
    if ($public_file_path !== NULL) {
      $public_file_path = str_starts_with($public_file_path, '/')
        ? $public_file_path . '/*'
        : '/' . $public_file_path . '/*';
      $default_exceptions[] = $public_file_path;
    }

    // Get a list of pages to never display agreements on.
    $exceptions = array_reduce($agreement_types, function ($result, Agreement $item) {
      $result[] = $item->get('path');
      return $result;
    }, $default_exceptions);

    $exception_string = implode("\n", $exceptions);
    if ($this->pathMatcher->matchPath($path, $exception_string)) {
      return FALSE;
    }

    // Reduce the agreement types based on the user role.
    $agreements_with_roles = array_reduce($agreement_types, function ($result, Agreement $item) use ($account) {
      if ($item->accountHasAgreementRole($account)) {
        $result[] = $item;
      }
      return $result;
    }, []);

    // Try to find an agreement type that matches the path.
    $pathMatcher = $this->pathMatcher;
    $self = $this;
    $info = array_reduce($agreements_with_roles, function ($result, Agreement $item) use ($account, $path, $pathMatcher, $self) {
      if ($result) {
        // Always returns the first matched agreement.
        return $result;
      }

      $has_agreed = $self->hasAgreed($item, $account);
      if ($self->agreementAppliesToPath($item, $path) && !$has_agreed) {
        $result = $item;
      }

      return $result;
    }, FALSE);

    $alter_context = [
      'path' => $path,
      'types' => $agreement_types,
      'exceptions' => $default_exceptions,
      'handler' => $self,
    ];
    $this->moduleHandler->alter('agreement_handler', $info, $account, $alter_context);

    return $info ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function agreementAppliesToPath(Agreement $agreement, string $path): bool {
    $pattern = $agreement->getVisibilityPages();
    $has_match = $this->pathMatcher->matchPath($path, $pattern);
    $visibility = (int) $agreement->getVisibilitySetting();
    if (0 === $visibility && FALSE === $has_match) {
      // An agreement exists that matches any page.
      return TRUE;
    }
    elseif (1 === $visibility && $has_match) {
      // An agreement exists that matches the current path.
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function prefixPath($value) {
    return $value ? '/' . $value : $value;
  }

  /**
   * Accept agreement for an anonymous user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to agree.
   * @param \Drupal\agreement\Entity\Agreement $agreement
   *   The agreement that the user is agreeing to.
   * @param int $agreed
   *   An optional integer to set the agreement status to. Defaults to 1.
   *
   * @return \Symfony\Component\HttpFoundation\Cookie
   *   A cookie to retain the user's acceptance of the agreement.
   */
  protected function agreeAnonymously(AccountInterface $account, Agreement $agreement, $agreed) {
    $agreementType = $agreement->id();
    $cookieName = static::ANON_AGREEMENT_COOKIE_PREFIX . $agreementType;
    $expire = 0;
    if ($agreement->getSettings()['frequency'] == 365) {
      $expire = new \DateTime('+1 year');
    }
    elseif ($agreement->agreeOnce()) {
      $expire = new \DateTime('+10 years');
    }
    return Cookie::create($cookieName, $agreed, $expire, '/', NULL, NULL, 'lax');
  }

  /**
   * Accept agreement for an authenticated user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account that is agreeing.
   * @param \Drupal\agreement\Entity\Agreement $agreement
   *   The agreement that the user is agreeing to.
   * @param int $agreed
   *   An optional integer to set the agreement status to. Defaults to 1.
   *
   * @return bool
   *   TRUE if the operation was successful. Otherwise FALSE.
   */
  protected function agreeWhileLoggedIn(AccountInterface $account, Agreement $agreement, $agreed) {
    try {
      $transaction = $this->connection->startTransaction();

      $this->connection->delete('agreement')
        ->condition('uid', $account->id())
        ->condition('type', $agreement->id())
        ->execute();

      $id = $this->connection->insert('agreement')
        ->fields([
          'uid' => $account->id(),
          'type' => $agreement->id(),
          'agreed' => $agreed,
          'sid' => session_id(),
          'agreed_date' => $this->time->getRequestTime(),
        ])
        ->execute();
    }
    catch (DatabaseExceptionWrapper $e) {
      $transaction->rollback();
      return FALSE;
    }
    catch (\Exception $e) {
      $transaction->rollback();
      return FALSE;
    }

    return isset($id);
  }

  /**
   * Check the status of the anonymous user for a particular agreement.
   *
   * @param \Drupal\agreement\Entity\Agreement $agreement
   *   The agreement to check if a user has agreed.
   *
   * @return bool
   *   TRUE if the user account has agreed to this agreement.
   */
  protected function hasAnonymousUserAgreed(Agreement $agreement) {
    $agreementType = $agreement->id();
    return $this->requestStack->getCurrentRequest()->cookies->has(static::ANON_AGREEMENT_COOKIE_PREFIX . $agreementType);
  }

  /**
   * Check the status of a user account for a particular agreement.
   *
   * @param \Drupal\agreement\Entity\Agreement $agreement
   *   The agreement to check if a user has agreed.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to check.
   *
   * @return bool
   *   TRUE if the user account has agreed to this agreement.
   */
  protected function hasAuthenticatedUserAgreed(Agreement $agreement, AccountInterface $account) {
    $settings = $agreement->getSettings();
    $frequency = $settings['frequency'];

    $query = $this->connection->select('agreement');
    $query
      ->fields('agreement', ['agreed'])
      ->condition('uid', $account->id())
      ->condition('type', $agreement->id())
      ->range(0, 1);

    if ($frequency == 0) {
      // Must agree on every login.
      $query->condition('sid', session_id());
    }
    else {
      // Must agree when frequency is set greater than zero (number of days).
      $timestamp = $agreement->getAgreementFrequencyTimestamp();
      if ($timestamp > 0) {
        $query->condition('agreed_date', $agreement->getAgreementFrequencyTimestamp(), '>=');
      }
    }

    $agreed = $query->execute()->fetchField();
    return $agreed !== NULL && $agreed > 0;
  }

}
