<?php
// @codingStandardsIgnoreFile
/**
 * @file
 * A database agnostic dump for testing purposes.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();

$connection->insert('system')
  ->fields([
    'filename',
    'name',
    'type',
    'owner',
    'status',
    'bootstrap',
    'schema_version',
    'weight',
    'info',
  ])
  ->values([
    'filename' => 'sites/all/modules/contrib/agreement/agreement.module',
    'name' => 'agreement',
    'type' => 'module',
    'owner' => '',
    'status' => '1',
    'bootstrap' => '0',
    'schema_version' => '7205',
    'weight' => '9999',
    'info' => 'a:13:{s:4:\"name\";s:9:\"Agreement\";s:11:\"description\";s:83:\"Module allows administrators to display an agreement to users of a particular role.\";s:4:\"core\";s:3:\"7.x\";s:9:\"configure\";s:29:\"admin/config/people/agreement\";s:5:\"files\";a:1:{i:0;s:14:\"agreement.test\";}s:7:\"version\";s:14:\"7.x-2.0-alpha4\";s:7:\"project\";s:9:\"agreement\";s:9:\"datestamp\";s:10:\"1568057888\";s:5:\"mtime\";i:1568057888;s:12:\"dependencies\";a:0:{}s:7:\"package\";s:5:\"Other\";s:3:\"php\";s:5:\"5.2.4\";s:9:\"bootstrap\";i:0;}'
  ])
  ->execute();

$connection->schema()->createTable('agreement', array(
  'fields' => array(
    'id' => array(
      'type' => 'serial',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ),
    'uid' => array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ),
    'agreed' => array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ),
    'sid' => array(
      'type' => 'varchar',
      'length' => 46,
    ),
    'agreed_date' => array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ),
  ),
  'primary key' => array('id'),
));

$connection->insert('agreement')
  ->fields(array('uid', 'agreed', 'sid', 'agreed_date'))
  ->values(array(2, 1, '', 1444945097))
  ->execute();
