<?php
// @codingStandardsIgnoreFile
/**
 * @file
 * A database agnostic dump for testing purposes.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();

$connection->insert('system')
  ->fields([
    'filename',
    'name',
    'type',
    'owner',
    'status',
    'bootstrap',
    'schema_version',
    'weight',
    'info',
  ])
  ->values([
    'filename' => 'sites/all/modules/contrib/agreement/agreement.module',
    'name' => 'agreement',
    'type' => 'module',
    'owner' => '',
    'status' => '1',
    'bootstrap' => '0',
    'schema_version' => '7205',
    'weight' => '9999',
    'info' => 'a:13:{s:4:\"name\";s:9:\"Agreement\";s:11:\"description\";s:83:\"Module allows administrators to display an agreement to users of a particular role.\";s:4:\"core\";s:3:\"7.x\";s:9:\"configure\";s:29:\"admin/config/people/agreement\";s:5:\"files\";a:1:{i:0;s:14:\"agreement.test\";}s:7:\"version\";s:14:\"7.x-2.0-alpha4\";s:7:\"project\";s:9:\"agreement\";s:9:\"datestamp\";s:10:\"1568057888\";s:5:\"mtime\";i:1568057888;s:12:\"dependencies\";a:0:{}s:7:\"package\";s:5:\"Other\";s:3:\"php\";s:5:\"5.2.4\";s:9:\"bootstrap\";i:0;}'
  ])
  ->execute();

$connection->schema()->createTable('agreement_type', array(
  'fields' => array(
    'id' => array(
      'type' => 'serial',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ),
    'name' => array(
      'type' => 'varchar',
      'length' => 100,
      'not null' => TRUE,
    ),
    'type' => array(
      'type' => 'varchar',
      'length' => 150,
      'not null' => TRUE,
    ),
    'path' => array(
      'type' => 'varchar',
      'length' => 150,
      'not null' => TRUE,
    ),
    'settings' => array(
      'type' => 'blob',
      'size' => 'big',
      'not null' => TRUE,
    ),
    'agreement' => array(
      'type' => 'text',
      'not null' => FALSE,
    ),
  ),
  'primary key' => array('id'),
  'unique keys' => array(
    'name' => array('name'),
    'path' => array('path'),
  ),
));

$connection->insert('agreement_type')
  ->fields(array('name', 'type', 'path', 'settings', 'agreement'))
  ->values(array(
    'name' => 'default',
    'type' => 'Default agreement',
    'path' => 'agreement',
    'settings' => serialize(array(
      'role' => array(2),
      'title' => 'Our agreement',
      'format' => 'filtered_html',
      'frequency' => 0,
      'success' => 'Thank you for accepting our agreement.',
      'failure' => 'You must accept our agreement to continue.',
      'revoked' => 'You successfully revoked your acceptance of our agreement',
      'checkbox' => 'I agree.',
      'submit' => 'Submit',
      'destination' => '',
      'visibility_settings' => 0,
      'visibility_pages' => '',
      'email_recipient' => '',
      'reset_date' => 0,
    )),
    'agreement' => 'Default agreement.',
  ))
  ->values(array(
     'name' => 'node_1_agreement',
     'type' => 'Node 1 agreement',
     'path' => 'agree-to-node-1',
     'settings' => serialize(array(
       // Keep one agreement with the old schema.
       'role' => 3,
       'title' => 'Node 1 agreement',
       'format' => 'filtered_html',
       'frequency' => -1,
       'success' => 'Thank you for accepting our agreement.',
       'failure' => 'You must accept our agreement to continue.',
       'revoked' => 'You successfully revoked your acceptance of our agreement',
       'checkbox' => 'I agree to node 1',
       'submit' => 'Agree',
       'destination' => 'node/1',
       'visibility_settings' => 1,
       'visibility_pages' => 'node/1',
       'email_recipient' => '',
       'reset_date' => 0,
     )),
     'agreement' => 'Agree to node 1.',
  ))
  ->execute();

$connection->schema()->createTable('agreement', array(
  'fields' => array(
    'id' => array(
      'type' => 'serial',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ),
    'type' => array(
      'type' => 'varchar',
      'length' => 100,
      'not null' => TRUE,
      'default' => 'default',
    ),
    'uid' => array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ),
    'agreed' => array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ),
    'sid' => array(
      'type' => 'varchar',
      'length' => 46,
    ),
    'agreed_date' => array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ),
  ),
  'primary key' => array('id'),
  'indexes' => array(
    'type_uid' => array('type', 'uid'),
  ),
));

$connection->insert('agreement')
  ->fields(array('type', 'uid', 'agreed', 'sid', 'agreed_date'))
  ->values(array('default', 2, 1, '', 1444945097))
  ->values(array('node_1_agreement', 3, 1, '', 0))
  ->execute();
