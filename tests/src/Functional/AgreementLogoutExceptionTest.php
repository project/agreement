<?php

namespace Drupal\Tests\agreement\Functional;

/**
 * Tests that a user logging out does not get agreement page.
 *
 * @group agreement
 */
class AgreementLogoutExceptionTest extends AgreementTestBase {

  /**
   * Asserts that no agreement page was reached when going to logout page.
   */
  public function testNoAgreement() {
    $account = $this->createUnprivilegedUser();
    $this->drupalLogin($account);

    $this->drupalGet('/user/logout');
    $this->assertNotAgreementPage($this->agreement);

    $this->submitForm([], 'Log out');
    $this->assertNotAgreementPage($this->agreement);
  }

}
