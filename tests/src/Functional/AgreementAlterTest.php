<?php

namespace Drupal\Tests\agreement\Functional;

/**
 * Tests a module that alters the agreement found for user and path.
 *
 * @group agreement
 */
class AgreementAlterTest extends AgreementTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'filter',
    'views',
    'agreement',
    'agreement_test',
  ];

  /**
   * Tests an agreement with two users.
   */
  public function testAgreement(): void {
    // Should be user with an even id.
    $userWithoutAgreement = $this->createUnprivilegedUser();
    // Should be user with an odd id.
    $userWithAgreement = $this->createUnprivilegedUser();
    // Should be user with id 4.
    $userWithoutTestAgreement = $this->createUnprivilegedUser();
    // Should be user with id 5.
    $userWithTestAgreement = $this->createUnprivilegedUser();

    // User was not sent to argeement page.
    $this->drupalLogin($userWithoutAgreement);
    $this->assertNotAgreementPage($this->agreement);

    // User was sent to agreement page.
    $this->drupalLogin($userWithAgreement);
    $this->assertAgreementPage($this->agreement);

    // Go to front page, no agreement.
    $this->drupalGet('/node');
    $this->assertNotAgreementPage($this->agreement);

    // Go anywhere else, open agreement.
    $this->drupalGet('/user/' . $userWithAgreement->id());
    $this->assertAgreementPage($this->agreement);

    // Try submitting agreement form.
    $this->assertAgreed($this->agreement);
    $this->assertNotAgreementPage($this->agreement);

    // Log in as user with id 5.
    $this->drupalLogin($userWithTestAgreement);
    $testAgreement = $this->container
      ->get('entity_type.manager')
      ->getStorage('agreement')
      ->load('test_agreement');

    $this->assertAgreementPage($testAgreement);

    // Try submitting agreement form manually without going to it since we
    // should already be on it.
    $settings = $testAgreement->getSettings();
    $this->submitForm(['agree' => 1], $settings['submit']);
    if ($this->checkForMetaRefresh()) {
      $this->metaRefreshCount = 0;
    }

    $this->assertSession()->pageTextContains($settings['success']);
    $this->assertNotAgreementPage($testAgreement);
    $this->assertNotAgreementPage($this->agreement);
  }

}
