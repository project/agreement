<?php

namespace Drupal\Tests\agreement\Functional;

/**
 * Tests agreement that applies to multiple roles.
 *
 * @group agreement
 */
class AgreementRedirectWithQueryTest extends AgreementTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $settings = $this->agreement->getSettings();
    $settings['visibility']['pages'] = ['<front>', '/user', '/user/*'];
    $this->agreement->set('settings', $settings);
    $this->agreement->save();
  }

  /**
   * Asserts that query parameters in agreement destination are maintained.
   */
  public function testAgreementWithQueryRedirect(): void {
    $settings = $this->agreement->getSettings();
    $account = $this->createUnprivilegedUser();

    // Login. The user should be on their user page, not the agreement page.
    $this->drupalLogin($account);
    $this->assertNotAgreementPage($this->agreement);

    // Go to a node and pass query parameters, should be on agreement page.
    $this->drupalGet('/node/1', ['query' => ['foo' => 'bar']]);
    $this->assertAgreementPage($this->agreement);

    // Accept the agreement.
    $this->submitForm(['agree' => 1], $settings['submit']);

    // Check for redirects. It's odd that drupalPostForm doesn't handle this but
    // drupalGet does.
    if ($this->checkForMetaRefresh()) {
      $this->metaRefreshCount = 0;
    }

    // Asserts that agreement is successful and that the URL is the node with
    // query parameters.
    $this->assertSession()->pageTextContains($settings['success']);
    $this->assertStringEndsWith('/node/1?foo=bar', $this->getUrl());
  }

}
