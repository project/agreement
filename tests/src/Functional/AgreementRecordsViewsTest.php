<?php

namespace Drupal\Tests\agreement\Functional;

/**
 * Tests the default view showing users that have agreed.
 *
 * @group agreement
 */
class AgreementRecordsViewsTest extends AgreementTestBase {

  /**
   * Asserts that there is a row for users whether they have agreed or not.
   */
  public function testAgreementRecordsView() {
    $unprivilegedUser = $this->createUnprivilegedUser();
    $this->drupalLogin($unprivilegedUser);

    // Sent to agreement page.
    $this->assertAgreementPage($this->agreement);

    // Try submitting agreement form.
    $this->assertAgreed($this->agreement);

    // Create a privileged user.
    $privilegedUser = $this->createPrivilegedUser();
    $this->drupalLogin($privilegedUser);

    // Go to the agreement configure page.
    $this->drupalGet('/admin/config/people/agreement/agreements');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContainsOnce('✔');
    $this->assertSession()->pageTextContains('✖');
  }

}
