<?php

namespace Drupal\Tests\agreement\Functional;

/**
 * Tests that user is redirected when attempting to access privileged route.
 *
 * @group agreement
 */
class AgreementAccessDeniedRedirectTest extends AgreementTestBase {

  /**
   * The user account to test.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $unprivilegedUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->unprivilegedUser = $this->createUnprivilegedUser();
  }

  /**
   * Asserts that the page is reached.
   */
  public function testAccessDeniedRedirect() {
    $this->drupalLogin($this->unprivilegedUser);
    $this->assertAgreementPage($this->agreement);

    $this->drupalGet('/admin');
    $this->assertAgreementPage($this->agreement);
  }

}
