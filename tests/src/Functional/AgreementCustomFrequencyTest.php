<?php

namespace Drupal\Tests\agreement\Functional;

/**
 * Tests custom frequencies.
 *
 * @group agreement
 */
class AgreementCustomFrequencyTest extends AgreementTestBase {

  /**
   * User account to test custom frequency.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $unprivilegedAccount;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Sets the frequency to a quarter of a day.
    $settings = $this->agreement->getSettings();
    $settings['frequency'] = .25;
    $this->agreement->set('settings', $settings);
    $this->agreement->save();

    $this->unprivilegedAccount = $this->createUnprivilegedUser();
  }

  /**
   * Asserts invalid custom frequency.
   */
  public function testInvalidCustomFrequency() {
    $this->drupalLogin($this->createPrivilegedUser());
    $this->drupalGet('/admin/config/people/agreement/manage/default');

    $edit = [
      'settings[frequency]' => '-0.25',
    ];

    $this->submitForm($edit, 'Save');
    $this->getSession()
      ->getPage()
      ->hasContent('Only -1 is an acceptable negative frequency.');
  }

  /**
   * Asserts agreement functionality.
   */
  public function testAgreement() {
    $this->drupalLogin($this->unprivilegedAccount);
    $this->assertAgreementPage($this->agreement);
    $this->assertAgreed($this->agreement);

    // Updates the user's agreed time te before the custom frequency.
    $moreThanSixHoursAgo = time() - 22000;
    \Drupal::database()->update('agreement')
      ->fields(['agreed_date' => $moreThanSixHoursAgo])
      ->condition('uid', $this->unprivilegedAccount->id())
      ->execute();

    $this->drupalGet('/node/2');
    $this->assertAgreementPage($this->agreement);
  }

}
