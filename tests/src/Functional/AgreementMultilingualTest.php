<?php

namespace Drupal\Tests\agreement\Functional;

use Drupal\agreement\Entity\Agreement;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests translating agreement.
 *
 * @group agreement
 */
class AgreementMultilingualTest extends AgreementTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'user',
    'language',
    'locale',
    'config_translation',
    'filter',
    'views',
    'agreement',
  ];

  /**
   * A user with permissions to administer.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Spanish user account to test with.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $spanishSpeaker;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Add a new language.
    ConfigurableLanguage::createFromLangcode('es')->save();

    $this->spanishSpeaker = $this->createUnprivilegedUser();
    $this->spanishSpeaker->set('preferred_langcode', 'es');
    $this->spanishSpeaker->save();

    // Create a user to administer languages.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'access content',
      'administer content types',
      'administer filters',
      'administer languages',
      'access user profiles',
      'bypass agreement',
      'translate configuration',
    ]);
    $this->drupalLogin($this->adminUser);

    // Enable URL and User language detection and selection.
    $languageEdit = [
      'language_interface[enabled][language-url]' => '1',
      'language_interface[enabled][language-user]' => '1',
    ];
    $this->drupalGet('admin/config/regional/language/detection');
    $this->submitForm($languageEdit, 'Save settings');

    // Asserts that the configuration is translatable.
    // @todo Make these better translations.
    $configEdit = [
      'translation[config_names][agreement.agreement.default][settings][title]' => 'Nuestro acuerdo',
      'translation[config_names][agreement.agreement.default][settings][checkbox]' => 'Estoy de acuerdo.',
      'translation[config_names][agreement.agreement.default][settings][submit]' => 'Enviar',
      'translation[config_names][agreement.agreement.default][settings][success]' => 'Gracias por aceptar nuestro acuerdo.',
      'translation[config_names][agreement.agreement.default][settings][revoked]' => 'Ha revocado con éxito su aceptación de nuestro acuerdo.',
      'translation[config_names][agreement.agreement.default][settings][failure]' => 'Debes aceptar nuestro acuerdo para continuar.',
      'translation[config_names][agreement.agreement.default][agreement]' => 'Este es el texto del acuerdo.',
    ];
    $this->drupalGet('admin/config/people/agreement/manage/default/translate/es/add');
    $this->submitForm($configEdit, 'Save translation');

    $this->drupalLogout();
  }

  /**
   * Asserts the text is changed to Spanish.
   */
  public function testMultilingualAgreement() {
    $this->drupalLogin($this->spanishSpeaker);

    // Sent to agreement page.
    $this->assertStringEndsWith($this->agreement->get('path'), $this->getUrl(), 'URL is agreement page.');

    // Found agreement text in Spanish.
    $this->assertSession()->titleEquals('Nuestro acuerdo | Drupal');
    $this->assertSession()->pageTextContains('Nuestro acuerdo');
    $this->assertSession()->checkboxNotChecked('Estoy de acuerdo.');
    $this->assertSession()->buttonExists('Enviar');
    $this->assertSession()->pageTextContainsOnce('Este es el texto del acuerdo.');

    $this->assertNotAgreed($this->agreement);
    $this->assertAgreed($this->agreement);
  }

  /**
   * Asserts that the user has agreed to the agreement.
   *
   * @param \Drupal\agreement\Entity\Agreement $agreement
   *   The agreement to agree to.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function assertAgreed(Agreement $agreement) {
    $this->drupalGet($agreement->get('path'));
    $this->submitForm(['agree' => 1], 'Enviar');

    $this->assertSession()->pageTextContains('Gracias por aceptar nuestro acuerdo.');
    $this->assertNotAgreementPage($agreement);
  }

  /**
   * Asserts that the user has not agreed to the agreement.
   *
   * @param \Drupal\agreement\Entity\Agreement $agreement
   *   The agreement to agree to.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function assertNotAgreed(Agreement $agreement) {
    $this->drupalGet($agreement->get('path'));
    $this->submitForm([], 'Enviar');
    $this->assertSession()->pageTextContains('Debes aceptar nuestro acuerdo para continuar.');
  }

}
