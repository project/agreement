<?php

namespace Drupal\Tests\agreement\Functional;

/**
 * Tests agreement configured for anonymous user.
 *
 * @group agreement
 */
class AgreementAnonymousTest extends AgreementTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $settings = $this->agreement->getSettings();
    $settings['roles'][] = 'anonymous';
    $this->agreement->set('settings', $settings);
    $this->agreement->save();
  }

  /**
   * Tests agreement as anonymous user.
   */
  public function testAgreement() {
    // Go to front page, no agreement.
    $this->drupalGet('/node');
    $this->assertNotAgreementPage($this->agreement);

    // Try submitting agreement form.
    $this->assertNotAgreed($this->agreement);
    $this->assertAgreed($this->agreement);
    $this->assertSession()->cookieEquals('agreement_anon_default', '1');
  }

}
