<?php

namespace Drupal\Tests\agreement\Kernel\EventSubscriber;

use Drupal\agreement\AgreementHandlerInterface;
use Drupal\agreement\EventSubscriber\AgreementSubscriber;
use Drupal\Core\Session\AccountInterface;
use Drupal\KernelTests\KernelTestBase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Tests the agreement route subscriber.
 *
 * @group agreement
 */
class AgreementSubscriberTest extends KernelTestBase {

  use ProphecyTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['filter', 'user', 'agreement'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    \Drupal::service('router.builder')->rebuild();
    $this->installConfig(['agreement']);
  }

  /**
   * Asserts that check for redirection method is functional.
   *
   * @param bool $canBypass
   *   Permission to bypass agreement.
   * @param bool $hasAgreement
   *   Whether or not an agreement is "found" in this test.
   * @param bool $expected
   *   Whether a redirect is expected or not.
   *
   * @dataProvider checkForRedirectionProvider
   */
  public function testCheckForRedirection($canBypass, $hasAgreement, $expected) {
    $pathProphet = $this->prophesize('\Drupal\Core\Path\CurrentPathStack');
    $pathProphet->getPath(Argument::any())->willReturn('test');

    $sessionProphet = $this->prophesize('\Drupal\Core\Session\SessionManagerInterface');

    $kernelProphet = $this->prophesize('\Drupal\Core\DrupalKernelInterface');

    $redirectionProphet = $this->prophesize('\Drupal\Core\Routing\RedirectDestinationInterface');
    $redirectionProphet->get()->willReturn('');
    $redirectionProphet->getAsArray()->willReturn(['destination' => '']);

    $request = new Request();
    // Gets the correct request type depending on Symfony version since 3.0.x
    // technically supports < Symfony 7.
    $request_type = defined('\Symfony\Component\HttpKernel\HttpKernelInterface::MAIN_REQUEST')
      ? constant('\Symfony\Component\HttpKernel\HttpKernelInterface::MAIN_REQUEST')
      : constant('\Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST');
    $event = new RequestEvent($kernelProphet->reveal(), $request, $request_type);

    $subscriber = new AgreementSubscriber(
      $this->getAgreementHandlerStub($hasAgreement),
      $pathProphet->reveal(),
      $sessionProphet->reveal(),
      $this->getAccountStub($canBypass),
      $redirectionProphet->reveal()
    );

    $subscriber->checkForRedirection($event);
    $isRedirect = $event->getResponse() ? $event->getResponse()->isRedirect() : FALSE;
    $this->assertEquals($expected, $isRedirect);
  }

  /**
   * Get the mocked current user account object.
   *
   * @param bool $canBypass
   *   Can the user bypass agreement.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The mocked user account object.
   */
  protected function getAccountStub($canBypass = FALSE): AccountInterface {
    $accountProphet = $this->prophesize('\Drupal\Core\Session\AccountInterface');
    $accountProphet->hasPermission('bypass agreement')->willReturn($canBypass);
    return $accountProphet->reveal();
  }

  /**
   * Get the mocked agreement handler.
   *
   * @param bool $willHaveAgreement
   *   Whether an agreement object should be returned or not.
   *
   * @return \Drupal\agreement\AgreementHandlerInterface
   *   The mocked agreement handler object.
   */
  protected function getAgreementHandlerStub($willHaveAgreement = FALSE): AgreementHandlerInterface {
    $agreement = FALSE;
    if ($willHaveAgreement) {
      $agreementProphet = $this->prophesize('\Drupal\agreement\Entity\Agreement');
      $agreementProphet->get('path')->willReturn('agreement');
      $agreementProphet->id()->willReturn('default');
      $agreement = $agreementProphet->reveal();
    }

    $handlerProphet = $this->prophesize('\Drupal\agreement\AgreementHandlerInterface');
    $handlerProphet
      ->getAgreementByUserAndPath(Argument::any(), Argument::any())
      ->willReturn($agreement);
    return $handlerProphet->reveal();
  }

  /**
   * Provides test arguments and expectations.
   *
   * @return array
   *   An array of test arguments.
   */
  public static function checkForRedirectionProvider(): array {
    return [
      // Bypass, Have agreement, Expected Response.
      [TRUE, FALSE, FALSE],
      [TRUE, TRUE, FALSE],
      [FALSE, FALSE, FALSE],
      [FALSE, TRUE, TRUE],
    ];
  }

}
