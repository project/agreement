<?php

/**
 * @file
 * API documentation for agreement module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alters the agreement entity that is returned for a given user and path.
 *
 * This alter hook will fire after all other processing of agreements has
 * occurred including path, role and whether the current user has agreed.
 *
 * However this alter hook will never be called if the current path matches any
 * of the default exceptions or one of the agreement paths.
 *
 * @param \Drupal\agreement\Entity\Agreement|false &$agreement
 *   The found agreement or false. The object can be altered, unset or replaced.
 * @param \Drupal\Core\Session\AccountProxyInterface $account
 *   The current user. This should not be altered.
 * @param array $context
 *   An associative array of contextual information including:
 *     - path: The path of the current request.
 *     - types: All of the agreement types.
 *     - exceptions: Paths that are excluded by default for all agreements.
 *     - handler: The Agreement Handler service.
 */
function hook_agreement_handler_alter(\Drupal\agreement\Entity\Agreement|false &$agreement, \Drupal\Core\Session\AccountProxyInterface $account, array $context): void {
  if ($account->id() % 2 === 0) {
    // User accounts with even-numbered ids do not get ANY agreements.
    $agreement = FALSE;
  }

  if ($agreement && $agreement->id() === 'default') {
    /** @var \Drupal\domain\DomainNegotiatorInterface $domainNegotiator */
    $domainNegotiator = \Drupal::service('domain.negotiator');
    if ($domainNegotiator->getActiveDomain()->id() === 'some_domain') {
      // Bypasses the agreement when the active domain is "some_domain".
      $agreement = FALSE;
    }
    elseif ($domainNegotiator->getActiveDomain()->id() === 'agreement_domain') {
      // Changes the agreement when the active domain is "agreement_domain", but
      // only if the user has not agreed and the user account has a role for the
      // agreement.
      /** @var \Drupal\agreement\Entity\Agreement $agreement_for_domain */
      $agreement_for_domain = $context['types']['agreement_for_domain'] ?? FALSE;
      $handler = \Drupal::service('agreement.handler');
      if ($agreement_for_domain && $handler->canAgree($agreement, $account)) {
        // Bypass the agreement if the user has agreed, otherwise show the
        // different agreement.
        $agreement = !$handler->hasAgreed($agreement_for_domain, $account)
          ? $agreement_for_domain
          : FALSE;
      }
    }
  }
}

/**
 * @} End of "addtogroup hooks".
 */
