<?php

/**
 * @file
 * Agreement module post-updates.
 */

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\ViewEntityInterface;

/**
 * Fixes translatable schema types from "string" to "label".
 */
function agreement_post_update_fix_translatable_schema(&$sandbox = NULL) {
  if (\Drupal::moduleHandler()->moduleExists('agreement')) {
    // Saving each agreement should be enough.
    \Drupal::classResolver(ConfigEntityUpdater::class)
      ->update($sandbox, 'agreement');
    return new TranslatableMarkup('Updated all agreements.');
  }
  return NULL;
}

/**
 * Changes frequency type from "integer" to "float".
 */
function agreement_post_update_update_frequency_type(&$sandbox = NULL) {
  if (\Drupal::moduleHandler()->moduleExists('agreement')) {
    \Drupal::classResolver(ConfigEntityUpdater::class)
      ->update($sandbox, 'agreement');
    return new TranslatableMarkup('Updated all agreements');
  }
  return NULL;
}

/**
 * Updates display id if it is null for all agreement views.
 */
function agreement_post_update_updates_default_views(&$sandbox = NULL) {
  if (\Drupal::moduleHandler()->moduleExists('agreement') &&
      \Drupal::moduleHandler()->moduleExists(('views'))) {
    // Updates views owned by agreement module that have displays with null ids.
    \Drupal::classResolver(ConfigEntityUpdater::class)
      ->update($sandbox, 'view', function (ViewEntityInterface $view) {
        if ($view->get('module') === 'agreement') {
          /** @var \Drupal\views\Plugin\views\display\DisplayPluginInterface[] $displays */
          $displays = $view->get('display');
          foreach ($displays as $id => $display) {
            if (!$display['id']) {
              $displays[$id]['id'] = $id;
            }
          }
          $view->set('display', $displays);

          return TRUE;
        }
        return FALSE;
      });
  }
  return NULL;
}
